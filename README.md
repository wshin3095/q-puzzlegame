1. Q-Puzzle game
2. To end the game will be to remove all the boxes through their corresponding 
colored doors.
3. It is puzzle game consisting of at most 2 colored doors (red and green) and 
corresponding 2 colored boxes. The number of doors and boxes may vary, however 
for every colored box there must be a matching colored door. 
4. The control pad will contain four directional buttons: Up, Down, Left, Right.
5. The player will select a box and use the control pad to move the box. The box 
will continue to move until it hits a wall or another box. If there is a door 
with the same color of the box in the direction of its movement, 
the box will be removed.

- Wan Jae Shin <wshin3095@conestogac.on.ca>

## Licence & copyright

&copy Wan Jae Shin

Licensed under the [MIT License](LICENSE)
