﻿namespace WShinAssignment2
{
    partial class DesignForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lblToolbox = new System.Windows.Forms.Label();
            this.pnlGenerate = new System.Windows.Forms.Panel();
            this.btnGenerate = new System.Windows.Forms.Button();
            this.txtColumns = new System.Windows.Forms.TextBox();
            this.lblColumns = new System.Windows.Forms.Label();
            this.txtRows = new System.Windows.Forms.TextBox();
            this.lblRows = new System.Windows.Forms.Label();
            this.pnlToolbox = new System.Windows.Forms.Panel();
            this.pnlMaze = new System.Windows.Forms.Panel();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.radGreenBox = new System.Windows.Forms.RadioButton();
            this.radRedBox = new System.Windows.Forms.RadioButton();
            this.radGreenDoor = new System.Windows.Forms.RadioButton();
            this.radRedDoor = new System.Windows.Forms.RadioButton();
            this.radWall = new System.Windows.Forms.RadioButton();
            this.radNone = new System.Windows.Forms.RadioButton();
            this.menuStrip.SuspendLayout();
            this.pnlGenerate.SuspendLayout();
            this.pnlToolbox.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(997, 28);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "menuStrip";
            // 
            // toolStripMenuItem
            // 
            this.toolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.toolStripMenuItem.Name = "toolStripMenuItem";
            this.toolStripMenuItem.Size = new System.Drawing.Size(46, 24);
            this.toolStripMenuItem.Text = "File";
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.exitToolStripMenuItem.Text = "Close";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // lblToolbox
            // 
            this.lblToolbox.AutoSize = true;
            this.lblToolbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblToolbox.Location = new System.Drawing.Point(47, 105);
            this.lblToolbox.Name = "lblToolbox";
            this.lblToolbox.Size = new System.Drawing.Size(67, 20);
            this.lblToolbox.TabIndex = 5;
            this.lblToolbox.Text = "Toolbox";
            // 
            // pnlGenerate
            // 
            this.pnlGenerate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlGenerate.Controls.Add(this.btnGenerate);
            this.pnlGenerate.Controls.Add(this.txtColumns);
            this.pnlGenerate.Controls.Add(this.lblColumns);
            this.pnlGenerate.Controls.Add(this.txtRows);
            this.pnlGenerate.Controls.Add(this.lblRows);
            this.pnlGenerate.Location = new System.Drawing.Point(0, 31);
            this.pnlGenerate.Name = "pnlGenerate";
            this.pnlGenerate.Size = new System.Drawing.Size(996, 58);
            this.pnlGenerate.TabIndex = 4;
            // 
            // btnGenerate
            // 
            this.btnGenerate.Location = new System.Drawing.Point(431, 14);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(107, 30);
            this.btnGenerate.TabIndex = 4;
            this.btnGenerate.Text = "Generate";
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // txtColumns
            // 
            this.txtColumns.Location = new System.Drawing.Point(292, 19);
            this.txtColumns.Name = "txtColumns";
            this.txtColumns.Size = new System.Drawing.Size(100, 22);
            this.txtColumns.TabIndex = 3;
            // 
            // lblColumns
            // 
            this.lblColumns.AutoSize = true;
            this.lblColumns.Location = new System.Drawing.Point(217, 19);
            this.lblColumns.Name = "lblColumns";
            this.lblColumns.Size = new System.Drawing.Size(66, 17);
            this.lblColumns.TabIndex = 2;
            this.lblColumns.Text = "Columns:";
            // 
            // txtRows
            // 
            this.txtRows.Location = new System.Drawing.Point(81, 19);
            this.txtRows.Name = "txtRows";
            this.txtRows.Size = new System.Drawing.Size(100, 22);
            this.txtRows.TabIndex = 1;
            // 
            // lblRows
            // 
            this.lblRows.AutoSize = true;
            this.lblRows.Location = new System.Drawing.Point(28, 19);
            this.lblRows.Name = "lblRows";
            this.lblRows.Size = new System.Drawing.Size(46, 17);
            this.lblRows.TabIndex = 0;
            this.lblRows.Text = "Rows:";
            // 
            // pnlToolbox
            // 
            this.pnlToolbox.Controls.Add(this.radGreenBox);
            this.pnlToolbox.Controls.Add(this.radRedBox);
            this.pnlToolbox.Controls.Add(this.radGreenDoor);
            this.pnlToolbox.Controls.Add(this.radRedDoor);
            this.pnlToolbox.Controls.Add(this.radWall);
            this.pnlToolbox.Controls.Add(this.radNone);
            this.pnlToolbox.Location = new System.Drawing.Point(12, 139);
            this.pnlToolbox.Name = "pnlToolbox";
            this.pnlToolbox.Size = new System.Drawing.Size(151, 346);
            this.pnlToolbox.TabIndex = 6;
            // 
            // pnlMaze
            // 
            this.pnlMaze.Location = new System.Drawing.Point(195, 120);
            this.pnlMaze.Name = "pnlMaze";
            this.pnlMaze.Size = new System.Drawing.Size(770, 700);
            this.pnlMaze.TabIndex = 7;
            // 
            // radGreenBox
            // 
            this.radGreenBox.Appearance = System.Windows.Forms.Appearance.Button;
            this.radGreenBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.radGreenBox.Image = global::WShinAssignment2.Properties.Resources.GreenBox39;
            this.radGreenBox.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.radGreenBox.Location = new System.Drawing.Point(6, 285);
            this.radGreenBox.Name = "radGreenBox";
            this.radGreenBox.Size = new System.Drawing.Size(136, 51);
            this.radGreenBox.TabIndex = 5;
            this.radGreenBox.TabStop = true;
            this.radGreenBox.Tag = "5";
            this.radGreenBox.Text = "Green Box";
            this.radGreenBox.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radGreenBox.UseVisualStyleBackColor = true;
            // 
            // radRedBox
            // 
            this.radRedBox.Appearance = System.Windows.Forms.Appearance.Button;
            this.radRedBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.radRedBox.Image = global::WShinAssignment2.Properties.Resources.RedBox39;
            this.radRedBox.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.radRedBox.Location = new System.Drawing.Point(6, 230);
            this.radRedBox.Name = "radRedBox";
            this.radRedBox.Size = new System.Drawing.Size(136, 51);
            this.radRedBox.TabIndex = 4;
            this.radRedBox.TabStop = true;
            this.radRedBox.Tag = "4";
            this.radRedBox.Text = "Red Box";
            this.radRedBox.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radRedBox.UseVisualStyleBackColor = true;
            // 
            // radGreenDoor
            // 
            this.radGreenDoor.Appearance = System.Windows.Forms.Appearance.Button;
            this.radGreenDoor.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.radGreenDoor.Image = global::WShinAssignment2.Properties.Resources.GreenDoor39;
            this.radGreenDoor.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.radGreenDoor.Location = new System.Drawing.Point(6, 175);
            this.radGreenDoor.Name = "radGreenDoor";
            this.radGreenDoor.Size = new System.Drawing.Size(136, 51);
            this.radGreenDoor.TabIndex = 3;
            this.radGreenDoor.TabStop = true;
            this.radGreenDoor.Tag = "3";
            this.radGreenDoor.Text = "Green Door";
            this.radGreenDoor.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radGreenDoor.UseVisualStyleBackColor = true;
            // 
            // radRedDoor
            // 
            this.radRedDoor.Appearance = System.Windows.Forms.Appearance.Button;
            this.radRedDoor.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.radRedDoor.Image = global::WShinAssignment2.Properties.Resources.RedDoor39;
            this.radRedDoor.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.radRedDoor.Location = new System.Drawing.Point(6, 120);
            this.radRedDoor.Name = "radRedDoor";
            this.radRedDoor.Size = new System.Drawing.Size(136, 51);
            this.radRedDoor.TabIndex = 2;
            this.radRedDoor.TabStop = true;
            this.radRedDoor.Tag = "2";
            this.radRedDoor.Text = "Red Door";
            this.radRedDoor.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radRedDoor.UseVisualStyleBackColor = true;
            // 
            // radWall
            // 
            this.radWall.Appearance = System.Windows.Forms.Appearance.Button;
            this.radWall.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.radWall.Image = global::WShinAssignment2.Properties.Resources.Wall39;
            this.radWall.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.radWall.Location = new System.Drawing.Point(6, 65);
            this.radWall.Name = "radWall";
            this.radWall.Size = new System.Drawing.Size(136, 51);
            this.radWall.TabIndex = 1;
            this.radWall.TabStop = true;
            this.radWall.Tag = "1";
            this.radWall.Text = "Wall";
            this.radWall.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radWall.UseVisualStyleBackColor = true;
            // 
            // radNone
            // 
            this.radNone.Appearance = System.Windows.Forms.Appearance.Button;
            this.radNone.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.radNone.Image = global::WShinAssignment2.Properties.Resources.None39;
            this.radNone.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.radNone.Location = new System.Drawing.Point(6, 10);
            this.radNone.Name = "radNone";
            this.radNone.Size = new System.Drawing.Size(136, 51);
            this.radNone.TabIndex = 0;
            this.radNone.TabStop = true;
            this.radNone.Tag = "0";
            this.radNone.Text = "None";
            this.radNone.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radNone.UseVisualStyleBackColor = true;
            // 
            // DesignForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(997, 833);
            this.Controls.Add(this.pnlMaze);
            this.Controls.Add(this.pnlToolbox);
            this.Controls.Add(this.lblToolbox);
            this.Controls.Add(this.pnlGenerate);
            this.Controls.Add(this.menuStrip);
            this.MainMenuStrip = this.menuStrip;
            this.Name = "DesignForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Design Form";
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.pnlGenerate.ResumeLayout(false);
            this.pnlGenerate.PerformLayout();
            this.pnlToolbox.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.Label lblToolbox;
        private System.Windows.Forms.Panel pnlGenerate;
        private System.Windows.Forms.Button btnGenerate;
        private System.Windows.Forms.TextBox txtColumns;
        private System.Windows.Forms.Label lblColumns;
        private System.Windows.Forms.TextBox txtRows;
        private System.Windows.Forms.Label lblRows;
        private System.Windows.Forms.Panel pnlToolbox;
        private System.Windows.Forms.RadioButton radGreenBox;
        private System.Windows.Forms.RadioButton radRedBox;
        private System.Windows.Forms.RadioButton radGreenDoor;
        private System.Windows.Forms.RadioButton radRedDoor;
        private System.Windows.Forms.RadioButton radWall;
        private System.Windows.Forms.RadioButton radNone;
        private System.Windows.Forms.Panel pnlMaze;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
    }
}