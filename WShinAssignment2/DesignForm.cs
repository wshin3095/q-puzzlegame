﻿/*
 *  Course      : PROG2370-20F-Sec4
 *  Task        : Assignment 3
 *  Name        : Wan Jae Shin
 *  StudentId   : 6873095
 *  Date        : Dec 6, 2020
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WShinAssignment2
{
    /// <summary>
    /// Maze Designer Form class of QGame to build a maze and save features
    /// </summary>
    public partial class DesignForm : Form
    {
        const int MIN_NUM = 5;      // Minimum number of maze grid
        const int MAX_NUM = 11;     // Maximum number of maze grid

        private int numberRows = 0;     // Number of rows to be entered from user
        private int numberColumns = 0;  // number of columns to be entered from user

        private PictureBox[,] picBlock;     // 2D array of PictureBoxs
        PictureBox activePicBlock = null;   // Selected a pictureBox

        // Images loaded the image through resources
        private Image imgWall = Properties.Resources.Wall39;
        private Image imgRedDoor = Properties.Resources.RedDoor39;
        private Image imgGreenDoor = Properties.Resources.GreenDoor39;
        private Image imgRedBox = Properties.Resources.RedBox39;
        private Image imgGreenBox = Properties.Resources.GreenBox39;

        Wall wall;                  // Declare Wall class
        List<Wall> wallList;        // Declare Wall list to store walls
        Door door;                  // Declare Door class
        List<Door> doorList;        // Declare Door list to store doors
        Box box;                    // Declare Box class
        List<Box> boxList;          // Declare Box list to store boxes

        /// <summary>
        /// A default constructor of Maze Designer Form class
        /// </summary>
        public DesignForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Close the Maze Designer Form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Do you want to close this form?", "QGame",
                                                MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                this.Close();
            }
        }

        /// <summary>
        /// Save the features of maze grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (activePicBlock != null)
            {
                saveFileDialog.Filter = "Text File (*.qgame)|*.qgame";
                saveFileDialog.FileName = "savegame1.qgame";
                saveFileDialog.RestoreDirectory = true;

                DialogResult result = saveFileDialog.ShowDialog();
                if (result == DialogResult.OK)
                {
                    wallList = new List<Wall>();
                    doorList = new List<Door>();
                    boxList = new List<Box>();

                    string userSelectedFile = saveFileDialog.FileName;

                    using (StreamWriter writer = new StreamWriter(userSelectedFile))
                    {
                        writer.WriteLine(numberRows.ToString());
                        writer.WriteLine(numberColumns.ToString());
                        for (int i = 0; i < numberRows; i++)
                        {
                            for (int j = 0; j < numberColumns; j++)
                            {
                                writer.WriteLine(i.ToString());
                                writer.WriteLine(j.ToString());
                                if (picBlock[i, j].Image == imgWall)
                                {
                                    writer.WriteLine(radWall.TabIndex.ToString());
                                    wall = new Wall(i, j, radWall.TabIndex);
                                    wallList.Add(wall);
                                }
                                else if (picBlock[i, j].Image == imgRedDoor)
                                {
                                    writer.WriteLine(radRedDoor.TabIndex.ToString());
                                    door = new Door(i, j, radRedDoor.TabIndex);
                                    doorList.Add(door);
                                }
                                else if (picBlock[i, j].Image == imgGreenDoor)
                                {
                                    writer.WriteLine(radGreenDoor.TabIndex.ToString());
                                    door = new Door(i, j, radGreenDoor.TabIndex);
                                    doorList.Add(door);
                                }
                                else if (picBlock[i, j].Image == imgRedBox)
                                {
                                    writer.WriteLine(radRedBox.TabIndex.ToString());
                                    box = new Box(i, j, radRedBox.TabIndex);
                                    boxList.Add(box);
                                }
                                else if (picBlock[i, j].Image == imgGreenBox)
                                {
                                    writer.WriteLine(radGreenBox.TabIndex.ToString());
                                    box = new Box(i, j, radGreenBox.TabIndex);
                                    boxList.Add(box);
                                }
                                else
                                {
                                    writer.WriteLine(radNone.TabIndex.ToString());
                                }
                            }
                        }
                    }

                    // Display result of a save file
                    MessageBox.Show("File saved successfully!\r\nTotal number of walls: "
                                    + wallList.Count.ToString() + "\r\nTotal number of doors: "
                                    + doorList.Count.ToString() + "\r\nTotal number of boxes: "
                                    + boxList.Count.ToString(), "QGame");
                }
            }
            else
            {
                MessageBox.Show("There is no design form", "QGame");
            }
        }

        /// <summary>
        /// Create a maze grid using number of rows and columns enterd from user
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnGenerate_Click(object sender, EventArgs e)
        {
            // Check number of rows and columns whether integer
            if ((int.TryParse(txtRows.Text, out numberRows) == false)
                || (int.TryParse(txtColumns.Text, out numberColumns) == false))
            {
                // If the numbers is not integer, show error message
                MessageBox.Show("Please provide valid data for rows and columns (both must be integers)",
                    "QGame", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtRows.Focus();
            }
            else
            {
                // If the numbers is integer, check whether the numbers between minimum and maximum
                if (((numberRows >= MIN_NUM) && (numberRows <= MAX_NUM))
                    && ((numberColumns >= MIN_NUM) && (numberColumns <= MAX_NUM)))
                {
                    // if already exist the grid picture box
                    if (picBlock != null)
                    {
                        DialogResult result = MessageBox.Show("Do you want to clear the grid Box?", "QGame",
                                                MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (result == DialogResult.Yes)
                        {
                            // Clear the grid picture box and create new grid
                            pnlMaze.Controls.Clear();
                            CreateGridPictureBox();
                        }
                    }
                    else
                    {
                        // Create new grid picture box
                        CreateGridPictureBox();
                    }
                }
                else
                {
                    // If the numbers is not between minimum and maximum, show error message
                    MessageBox.Show("Please re-enter the number between 6 and 11 rows and columns", "QGame",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtRows.Focus();
                }
            }
        }

        /// <summary>
        /// Create new grid picture box
        /// </summary>
        private void CreateGridPictureBox()
        {
            picBlock = new PictureBox[numberRows, numberColumns];

            for (int i = 0; i < numberRows; i++)
            {
                for (int j = 0; j < numberColumns; j++)
                {
                    picBlock[i, j] = new PictureBox();
                    picBlock[i, j].Location = new Point((50 * j), (50 * i));
                    picBlock[i, j].Size = new Size(50, 50);
                    
                    picBlock[i, j].Image = null;

                    picBlock[i, j].SizeMode = PictureBoxSizeMode.Zoom;
                    picBlock[i, j].BorderStyle = BorderStyle.FixedSingle;
                    picBlock[i, j].Tag = i.ToString() + "," + j.ToString();     // Set 2D array position (x,y)
                    pnlMaze.Controls.Add(picBlock[i, j]);

                    picBlock[i, j].Click += new EventHandler(PicBlockClicked);  
                }
            }
        }


        /// <summary>
        /// Click a maze grid PictureBox 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PicBlockClicked(object sender, EventArgs e)
        {
            activePicBlock = (PictureBox)sender;
            
            string[] selectTags = activePicBlock.Tag.ToString().Split(',');

            if ((int.TryParse(selectTags[0], out int activeRow))
                && (int.TryParse(selectTags[1], out int activeColumn)))
            {
                if (radWall.Checked == true)
                {
                    picBlock[activeRow, activeColumn].Image = imgWall;
                }
                else if (radRedDoor.Checked == true)
                {
                    picBlock[activeRow, activeColumn].Image = imgRedDoor;
                }
                else if (radGreenDoor.Checked == true)
                {
                    picBlock[activeRow, activeColumn].Image = imgGreenDoor;
                }
                else if (radRedBox.Checked == true)
                {
                    picBlock[activeRow, activeColumn].Image = imgRedBox;
                }
                else if (radGreenBox.Checked == true)
                {
                    picBlock[activeRow, activeColumn].Image = imgGreenBox;
                }
                else if (radNone.Checked == true)
                {
                    picBlock[activeRow, activeColumn].Image = null;
                }
                else
                {
                    MessageBox.Show("Select a toolbox", "QGame", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Error: PictureBox [" + activePicBlock.Tag.ToString() + "]", "QGame",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

    }
}
