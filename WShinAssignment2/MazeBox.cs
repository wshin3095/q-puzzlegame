﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WShinAssignment2
{
    public class MazeBox
    {
        public int NumberOfRow { get; set; }
        public int NumberOfColumn { get; set; }
        public int NumberOfType { get; set; }

        public MazeBox(int numberOfRow, int numberOfColumn, int numberOfType)
        {
            this.NumberOfRow = numberOfRow;
            this.NumberOfColumn = numberOfColumn;
            this.NumberOfType = numberOfType;
        }
    }
}
