﻿/*
 *  Course      : PROG2370-20F-Sec4
 *  Task        : Assignment 3
 *  Name        : Wan Jae Shin
 *  StudentId   : 6873095
 *  Date        : Dec 6, 2020
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WShinAssignment2
{
    public partial class PlayForm : Form
    {
        private int numberRows = 0;     // Number of rows to be entered from user
        private int numberColumns = 0;  // number of columns to be entered from user

        private PictureBox[,] picBlock;     // 2D array of PictureBoxs
        PictureBox activePicBlock = null;   // Selected a pictureBox

        const int BLOCK_WALL = 1;       // Type of block wall
        const int RED_DOOR = 2;         // Type of red door
        const int GREEN_DOOR = 3;       // Type of green door
        const int RED_BOX = 4;          // Type of red box
        const int GREEN_BOX = 5;        // Type of green box
        const int SIZE_BOX = 50;        // Size of boxes

        // Images loaded the image through resources
        private Image imgWall = Properties.Resources.Wall39;
        private Image imgRedDoor = Properties.Resources.RedDoor39;
        private Image imgGreenDoor = Properties.Resources.GreenDoor39;
        private Image imgRedBox = Properties.Resources.RedBox39;
        private Image imgGreenBox = Properties.Resources.GreenBox39;
        private Image imgActiveRedBox = Properties.Resources.RedBox39A; // selected box
        private Image imgActiveGreenBox = Properties.Resources.GreenBox39A; // selected box

        MazeBox mazeBox;            // Declare MazeBox class
        List<MazeBox> mazeBoxList;  // Declare MazeBox list to store maze boxes

        private Timer timer;        // Declare Timer
        const int INTERVAL = 50;    // Decalre interval of timer
        const int NONE_VALUE = -1;  // Declare default value
        private int collisionRow = NONE_VALUE;               // Row of a collision box 
        private int collisionColumn = NONE_VALUE;            // Column of a collision box 
        private int collisionMazeListBoxIndex = NONE_VALUE;  // Index of a collision box 

        private int activeRow = NONE_VALUE;               // Row of active box selected
        private int activeColumn = NONE_VALUE;            // Column of active box selected
        private int activeType = NONE_VALUE;              // Type of active box (type of box and color)
        private int activeMazeListBoxIndex = NONE_VALUE;  // Index of active box in mazeListBox

        private int numberOfMoves = 0;      // Count of move of boxes
        private int numberOfBoxes = 0;      // Count of boxes existed

        public PlayForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Load form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PlayForm_Load(object sender, EventArgs e)
        {
            pnlButtons.Enabled = false;
        }

        /// <summary>
        /// Close this form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Do you want to close this form?", "QGame",
                                    MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                this.Close();
            }
        }

        /// <summary>
        /// Load a game from files already designed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void loadGameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Check exist a game loaded
            bool newGame = true;
            if (pnlButtons.Enabled == true)
            {
                DialogResult resultLoad = MessageBox.Show("Do you want to load a new game?", "QGame",
                                                MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (resultLoad == DialogResult.Yes)
                {
                    pnlMaze.Controls.Clear();
                    numberOfBoxes = 0;
                    numberOfMoves = 0;
                }
                else
                {
                    newGame = false;
                }
            }

            if (newGame == true)
            {
                // load a new game
                openFileDialog.Filter = "Text File (*.qgame)|*.qgame";
                openFileDialog.FileName = "savegame1.qgame";
                openFileDialog.RestoreDirectory = true;

                DialogResult result = openFileDialog.ShowDialog();
                if (result == DialogResult.OK)
                {
                    mazeBoxList = new List<MazeBox>();

                    string userSelectedFile = openFileDialog.FileName;

                    using (StreamReader reader = new StreamReader(userSelectedFile))
                    {
                        int.TryParse(reader.ReadLine(), out numberRows);
                        int.TryParse(reader.ReadLine(), out numberColumns);
                        picBlock = new PictureBox[numberRows, numberColumns];

                        int row = 0;
                        int column = 0;
                        int type = 0;

                        for (int i = 0; i < numberRows * numberColumns; i++)
                        {
                            // Read a data of box with row, column and type
                            int.TryParse(reader.ReadLine(), out row);
                            int.TryParse(reader.ReadLine(), out column);
                            int.TryParse(reader.ReadLine(), out type);

                            if (type == BLOCK_WALL)
                            {
                                // Create new grid picture box
                                CreateGridPictureBox(row, column);
                                picBlock[row, column].Image = imgWall;

                                mazeBox = new MazeBox(row, column, type);
                                mazeBoxList.Add(mazeBox);
                            }
                            else if (type == RED_DOOR)
                            {
                                CreateGridPictureBox(row, column);
                                picBlock[row, column].Image = imgRedDoor;

                                mazeBox = new MazeBox(row, column, type);
                                mazeBoxList.Add(mazeBox);
                            }
                            else if (type == GREEN_DOOR)
                            {
                                CreateGridPictureBox(row, column);
                                picBlock[row, column].Image = imgGreenDoor;

                                mazeBox = new MazeBox(row, column, type);
                                mazeBoxList.Add(mazeBox);
                            }
                            else if (type == RED_BOX)
                            {
                                CreateGridPictureBox(row, column);
                                picBlock[row, column].Image = imgRedBox;
                                picBlock[row, column].Click += new EventHandler(PicBlockClicked);

                                mazeBox = new MazeBox(row, column, type);
                                mazeBoxList.Add(mazeBox);
                                numberOfBoxes += 1;
                            }
                            else if (type == GREEN_BOX)
                            {
                                CreateGridPictureBox(row, column);
                                picBlock[row, column].Image = imgGreenBox;
                                picBlock[row, column].Click += new EventHandler(PicBlockClicked);

                                mazeBox = new MazeBox(row, column, type);
                                mazeBoxList.Add(mazeBox);
                                numberOfBoxes += 1;
                            }
                        }
                    }
                    // Display result of a read file
                    numberOfMoves = 0;
                    txtNumberOfMoves.Text = numberOfMoves.ToString();
                    txtNumberOfBoxes.Text = numberOfBoxes.ToString();
                    pnlButtons.Enabled = true;
                }
            }
        }

        /// <summary>
        /// Create new grid picture box
        /// </summary>
        /// <param name="row"></param>
        /// <param name="column"></param>
        private void CreateGridPictureBox(int row, int column)
        {
            picBlock[row, column] = new PictureBox();
            picBlock[row, column].Location = new Point((SIZE_BOX * column), (SIZE_BOX * row));
            picBlock[row, column].Size = new Size(SIZE_BOX, SIZE_BOX);             
            picBlock[row, column].SizeMode = PictureBoxSizeMode.Zoom;
            // Display boxes at grid
            pnlMaze.Controls.Add(picBlock[row, column]);
        }

        /// <summary>
        /// Click a box at maze grid 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PicBlockClicked(object sender, EventArgs e)
        {
            // Initialize images of boxes  
            if (activePicBlock != null)
            {
                if (activeType == RED_BOX)
                {
                    activePicBlock.Image = imgRedBox;
                }
                else if (activeType == GREEN_BOX)
                {
                    activePicBlock.Image = imgGreenBox;
                }
            }

            activePicBlock = (PictureBox)sender;

            // Calculate row and column of active box selected
            int valueOfLeft = activePicBlock.Left;
            int valueOfTop = activePicBlock.Top;
            activeColumn = valueOfLeft / SIZE_BOX;
            activeRow = valueOfTop / SIZE_BOX;

            // Find index of ative box selected
            activeMazeListBoxIndex = mazeBoxList.FindIndex(x => x.NumberOfRow == activeRow
                                    && x.NumberOfColumn == activeColumn);
            activeType = mazeBoxList[activeMazeListBoxIndex].NumberOfType;
 
            // Change border style of active box selected
            if (activeType == RED_BOX)
            {
                activePicBlock.Image = imgActiveRedBox;
            }
            else if (activeType == GREEN_BOX)
            {
                activePicBlock.Image = imgActiveGreenBox;
            }
        }

        /// <summary>
        /// Click the button to move active box to left
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnLeft_Click(object sender, EventArgs e)
        {
            if (activePicBlock == null)
            {
                MessageBox.Show("Click to select", "GGame", 
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                collisionColumn = NONE_VALUE;
                collisionMazeListBoxIndex = NONE_VALUE;
                // Find a collision column and index at left side from mazeListBox
                try
                {
                    collisionColumn = mazeBoxList.Where(r => r.NumberOfRow == activeRow
                                                        && r.NumberOfColumn < activeColumn)
                                                        .Max(r => r.NumberOfColumn);
                    // Find index of collision box
                    collisionMazeListBoxIndex = mazeBoxList.FindIndex(x => x.NumberOfRow == activeRow
                                                        && x.NumberOfColumn == collisionColumn);
                    // Check a collision index whether the same color door 
                    MoveBox();  // or move the box in order to timer
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Cannot move left!", "QGame",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        /// <summary>
        /// Click the button to move active box to right
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRight_Click(object sender, EventArgs e)
        {
            if (activePicBlock == null)
            {
                MessageBox.Show("Click to select", "GGame", 
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                collisionColumn = NONE_VALUE;
                collisionMazeListBoxIndex = NONE_VALUE;
                // Find a collision column and index at right side from mazeListBox
                try
                {
                    collisionColumn = mazeBoxList.Where(r => r.NumberOfRow == activeRow
                                                        && r.NumberOfColumn > activeColumn)
                                                        .Min(r => r.NumberOfColumn);
                    // Find index of collision box
                    collisionMazeListBoxIndex = mazeBoxList.FindIndex(x => x.NumberOfRow == activeRow
                                                    && x.NumberOfColumn == collisionColumn);
                    // Check a collision index whether the same color door 
                    MoveBox();  // or move the box in order to timer
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Cannot move right!", "QGame",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        /// <summary>
        /// Click the button to move active box to up
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnUp_Click(object sender, EventArgs e)
        {
            if (activePicBlock == null)
            {
                MessageBox.Show("Click to select", "GGame", 
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                collisionRow = NONE_VALUE;
                collisionMazeListBoxIndex = NONE_VALUE;
                // Find a collision row and index at up side from mazeListBox
                try
                {
                    collisionRow = mazeBoxList.Where(r => r.NumberOfColumn == activeColumn
                                                    && r.NumberOfRow < activeRow)
                                                    .Max(r => r.NumberOfRow);
                    // Find index of collision box
                    collisionMazeListBoxIndex = mazeBoxList.FindIndex(x => x.NumberOfRow == collisionRow
                                    && x.NumberOfColumn == activeColumn);
                    // Check a collision index whether the same color door 
                    MoveBox();  // or move the box in order to timer
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Cannot move up!", "QGame",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        /// <summary>
        /// Click the button to move active box to down
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDown_Click(object sender, EventArgs e)
        {
            if (activePicBlock == null)
            {
                MessageBox.Show("Click to select", "GGame", 
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                collisionRow = NONE_VALUE;
                collisionMazeListBoxIndex = NONE_VALUE;
                // Find a collision row and index at down side from mazeListBox
                try
                {
                    collisionRow = mazeBoxList.Where(r => r.NumberOfColumn == activeColumn
                                                    && r.NumberOfRow > activeRow)
                                                    .Min(r => r.NumberOfRow);
                    collisionMazeListBoxIndex = mazeBoxList.FindIndex(x => x.NumberOfRow == collisionRow
                                    && x.NumberOfColumn == activeColumn);
                    // Check a collision index whether the same color door 
                    MoveBox();  // or move the box in order to timer
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Cannot move down!", "QGame",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        /// <summary>
        /// Check same color door and move box inorder to timer
        /// </summary>
        private void MoveBox()
        {
            timer = new Timer();
            timer.Interval = INTERVAL;
            timer.Enabled = true;

            // Check a collision index whether the same color door 
            if (mazeBoxList[collisionMazeListBoxIndex].NumberOfType == (activeType - 2))
            {
                EscapeFromGrid();   // Escape active box from grid
            }
            else
            {
                if (btnLeft.Focused == true)
                {
                    if ((activeColumn - 1) > collisionColumn)  // Check space to move left
                    {
                        timer.Tick += Timer_Tick_Left;   // Move left the active box in order to timer
                    }
                    else
                    {
                        MessageBox.Show("Cannot move left", "QGame", 
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else if (btnRight.Focused == true)
                {
                    if ((activeColumn + 1) < collisionColumn)  // Check space to move right
                    {
                        timer.Tick += Timer_Tick_Right;   // Move right the active box in order to timer
                    }
                    else
                    {
                        MessageBox.Show("Cannot move right", "QGame",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else if (btnUp.Focused == true)
                {
                    if((activeRow - 1) > collisionRow)  // Check space to move up
                    { 
                        timer.Tick += Timer_Tick_Up;   // Move up the active box in order to timer
                    }
                    else
                    {
                        MessageBox.Show("Cannot move up", "QGame",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else if (btnDown.Focused == true)
                {
                    if((activeRow + 1) < collisionRow)  // Check space to move down
                    { 
                        timer.Tick += Timer_Tick_Down;   // Move down the active box in order to timer
                    }
                    else
                    {
                        MessageBox.Show("Cannot move down", "QGame",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
        }

        /// <summary>
        /// Escape active box from grid met a door of same color 
        /// </summary>
        private void EscapeFromGrid()
        {
            activePicBlock.Visible = false;
            activePicBlock = null;

            // Remove a active box from mazeBoxList
            mazeBoxList.RemoveAt(activeMazeListBoxIndex);
            numberOfMoves += 1;
            txtNumberOfMoves.Text = numberOfMoves.ToString();
            numberOfBoxes -= 1;
            txtNumberOfBoxes.Text = numberOfBoxes.ToString();

            // Check end of game
            if (numberOfBoxes == 0)
            {
                MessageBox.Show("Congratulations\r\nGame End", "QGame",
                                 MessageBoxButtons.OK, MessageBoxIcon.Information);
                pnlMaze.Controls.Clear();
                pnlButtons.Enabled = false;
                mazeBoxList.Clear();
            }
        }

        /// <summary>
        /// Move left the active box selected in order to timer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Timer_Tick_Left(object sender, EventArgs e)
        {
            if (activePicBlock.Left == ((collisionColumn * SIZE_BOX) + SIZE_BOX))
            {
                // Stop timer when the active box meet a collision box
                StopTimer();
            }
            else
            {
                // Move left a step the active box
                activePicBlock.Left -= SIZE_BOX;
                activeColumn -= 1;
            }
        }

        /// <summary>
        /// Move right the active box selected in order to timer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Timer_Tick_Right(object sender, EventArgs e)
        {
            if (activePicBlock.Left == ((collisionColumn * SIZE_BOX) - SIZE_BOX))
            {
                // Stop timer when the active box meet a collision box
                StopTimer();
            }
            else
            {
                // Move right a step the active box
                activePicBlock.Left += SIZE_BOX;
                activeColumn += 1;
            }
        }

        /// <summary>
        /// Move up the active box selected in order to timer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Timer_Tick_Up(object sender, EventArgs e)
        {
            if (activePicBlock.Top == ((collisionRow * SIZE_BOX) + SIZE_BOX))
            {
                // Stop when active box meet a collision box
                StopTimer();
            }
            else
            {
                // Move up a step the active box
                activePicBlock.Top -= SIZE_BOX;
                activeRow -= 1;
            }
        }

        /// <summary>
        /// Move down the active box selected in order to timer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Timer_Tick_Down(object sender, EventArgs e)
        {
            if (activePicBlock.Top == ((collisionRow * SIZE_BOX) - SIZE_BOX))
            {
                // Stop when the active box meet a collision box
                StopTimer();
            }
            else
            {
                // Move down a step the active box
                activePicBlock.Top += SIZE_BOX;
                activeRow += 1;
            }
        }
     
        /// <summary>
        /// Stop timer when the box meet a collision box
        /// </summary>
        private void StopTimer()
        {
            timer.Stop();
            timer.Enabled = false;
            numberOfMoves += 1;    // Add count of moves
            txtNumberOfMoves.Text = numberOfMoves.ToString();
            // Change row and column of active box
            mazeBoxList[activeMazeListBoxIndex].NumberOfRow = activeRow;
            mazeBoxList[activeMazeListBoxIndex].NumberOfColumn = activeColumn;
        }
    }
}
