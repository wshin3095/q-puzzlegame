﻿
/*
 *  Course      : PROG2370-20F-Sec4
 *  Task        : Assignment 3
 *  Name        : Wan Jae Shin
 *  StudentId   : 6873095
 *  Date        : Dec 6, 2020
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WShinAssignment2
{
    /// <summary>
    /// Control Panel Form class of QGame to design and play
    /// </summary>
    public partial class QGameControlForm : Form
    {
        /// <summary>
        /// a default constructor of Control Panel Form class
        /// </summary>
        public QGameControlForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Close the Control Panel Form class
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Create a Maze Designer Form 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDesign_Click(object sender, EventArgs e)
        {
            DesignForm designForm = new DesignForm();
            designForm.ShowDialog(this);
        }

        private void btnPlay_Click(object sender, EventArgs e)
        {
            PlayForm playForm = new PlayForm();
            playForm.ShowDialog(this);
        }
    }
}
